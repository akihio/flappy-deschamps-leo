using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBkR : MonoBehaviour
{
        private Vector3 rightBottomCameraBorder;

        private Vector3 siz;

        private float positionRestartX=0.0f;

        private Vector2 mouvement;

        public GameObject background1;
        public GameObject background2;
        public GameObject background3;
    // Start is called before the first frame update
    void Start()
    {
        rightBottomCameraBorder=Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        positionRestartX=background3.transform.position.x;
        mouvement=new Vector2(1, 0);
    }

    // Update is called once per frame
    void Update()
    {
        background1.GetComponent<Rigidbody2D>().velocity=mouvement;
        background2.GetComponent<Rigidbody2D>().velocity=mouvement;
        background3.GetComponent<Rigidbody2D>().velocity=mouvement;

        siz.x=background1.gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y=background1.gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if(background1.transform.position.x>rightBottomCameraBorder.x+(siz.x/2)){
            background1.transform.position=new Vector3(positionRestartX, background1.transform.position.y, background1.transform.position.z);
        }
        else if(background2.transform.position.x>rightBottomCameraBorder.x+(siz.x/2)){
            background2.transform.position=new Vector3(positionRestartX, background2.transform.position.y, background2.transform.position.z);
        }
        else if(background3.transform.position.x>rightBottomCameraBorder.x+(siz.x/2)){
            background3.transform.position=new Vector3(positionRestartX, background2.transform.position.y, background2.transform.position.z);
        }
    }
}
