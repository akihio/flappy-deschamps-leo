using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class birdBetweenPipes : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public static int score;
    // Start is called before the first frame update
    void Start()
    {
       score=0; 
    }
    void OnTriggerEnter2D(Collider2D collider){
        if(collider.gameObject.name == "box"){
            
            UpdateScore();
        }
    }

    void UpdateScore(){
        score=score+1;
        scoreText.text=score+"";
    }

    public void UpdateScoreTrois(){
        score=score+3;
        scoreText.text=score+"";
    }
}
