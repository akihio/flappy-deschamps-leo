using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour
{

    public Vector2 mouvement;
    public GameObject pipe1Up;
    public GameObject pipe1Down;
    public GameObject box1;

    private Transform pipe1UpOriginalTransform;
    private Transform pipe1DownOriginalTransform; 
    private Transform box1OriginalTransform; 

    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder;

    private Vector3 siz;
    private Vector3 boxsiz;
    // Start is called before the first frame update
    void Start()
    {
        rightBottomCameraBorder=Camera.main.ViewportToWorldPoint (new Vector3(1,0,0));
        leftBottomCameraBorder=Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        boxsiz.y=box1.gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
    }

    // Update is called once per frame
    void Update()
    {
        pipe1Up.GetComponent<Rigidbody2D>().velocity=mouvement;
        pipe1Down.GetComponent<Rigidbody2D>().velocity=mouvement;
        box1.GetComponent<Rigidbody2D>().velocity=mouvement;

        pipe1UpOriginalTransform=pipe1Up.transform;
        pipe1DownOriginalTransform=pipe1Down.transform;
        box1OriginalTransform=box1.transform;

        siz.x=pipe1Up.gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y=pipe1Up.gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if(pipe1Up.transform.position.x<leftBottomCameraBorder.x-(siz.x/2)){
            moveToRightPipe();
        }
    }

    void moveToRightPipe(){
        
        float randomY=Random.Range(1,4)-2;
        float posX=rightBottomCameraBorder.x+(siz.x/2);
        float posY=pipe1UpOriginalTransform.position.y+randomY;

        Vector3 tmpPos=new Vector3(posX, posY, pipe1Up.transform.position.z);
        pipe1Up.transform.position=tmpPos;

        posY=pipe1DownOriginalTransform.position.y+randomY;
        tmpPos=new Vector3(posX, posY, pipe1Down.transform.position.z);
        pipe1Down.transform.position=tmpPos;

        float boxY=pipe1Up.transform.position.y + (siz.y/2) + (boxsiz.y/2);
        box1.transform.position=new Vector3(posX, boxY ,  pipe1Down.transform.position.z);

    }
}
