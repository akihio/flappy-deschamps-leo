using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class collideManagementBird : MonoBehaviour
{
    public GameObject bird;
    private Vector3 leftBottomCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 siz;
    
    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder=Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
        leftTopCameraBorder=Camera.main.ViewportToWorldPoint (new Vector3(0,1,0));
    }

    // Update is called once per frame
    void Update()
    {
        siz.x=bird.gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        siz.y=bird.gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

        if (bird.transform.position.y<leftBottomCameraBorder.y- siz.y){
            endActions();
        }
        if(bird.transform.position.y>leftTopCameraBorder.y+siz.y){
            endActions();
        }
    }

    void OnTriggerEnter2D(Collider2D collider){
        if(collider.gameObject.name == "bird1"){
            
            bird.GetComponent<touchAction>().enabled=false;
        }
    }

    void endActions(){
        SceneManager.LoadScene("scene4-End");
    }
}
