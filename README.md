Deschamps Léo


Implanté : 
- Scène 1 :
	- Un titre 
	- Un bouton "Play" qui change sur la scène 2
	- Une image "Tap" qui apparait et disparait toutes les secondes
- Scène 2 :
	- Un oiseau qui vole en appuyant sur "Entrer"
	- L'oiseau qui passe entre les tuyau gagne 1 point
	- Si l'oiseau se cogne contre un mur il tombe et perd suivit d'un changement sur la scène 3
	- L'oiseau perd s'il va trop haut ou trop bas
- Scène 3 :
	- page Game over
	- Un bouton replay qui lance la scène 2

Jeu : Flappy Bird

Règle : Touche espace pour sauter
	    Touche Entrer gagne 3 points

Difficultés non résolues : Je n'ai pas réussi à faire la rotation de l'oiseau lorsqu'il touche un tuyau et j'aurai aimé envoyer le score final sur la dernière scène. Sur la scène 2, je voulais que lorsque l'on appuie sur Enter, l'oiseau fasse un looping et gagne 3 points mais je n'arrive pas à faire le looping.

devices testés : Ordinateur Windows
